// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::{borrow::Borrow, fs};

use crate::{Error, Result};
use isahc::{prelude::Configurable, ReadResponseExt};
use url::Url;

fn default_user_agent() -> String {
    format!("{}/{}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"))
}

#[derive(Clone)]
pub struct BlobDownloader {
    http_client: isahc::HttpClient,
}

impl BlobDownloader {
    pub fn new() -> Result<Self> {
        let http_client = isahc::HttpClientBuilder::new()
            // avoid sending Accept-Encoding as we mostly are downloading already compressed files
            .automatic_decompression(false)
            .default_header("User-Agent", default_user_agent())
            .build()?;
        Ok(Self { http_client })
    }

    pub fn new_with_http_client(http_client: isahc::HttpClient) -> Self {
        Self { http_client }
    }

    pub fn download_bytes<U: Borrow<Url>>(&self, url: U) -> Result<Vec<u8>> {
        let url: &Url = url.borrow();
        match url.scheme() {
            "file" => self.download_bytes_file(url),
            "http" | "https" => self.download_bytes_isahc(url),
            scheme => Err(Error::InvalidScheme(scheme.to_string())),
        }
    }

    fn download_bytes_file(&self, url: &Url) -> Result<Vec<u8>> {
        let path = url.to_file_path().map_err(|_| Error::InvalidFileURL)?;
        let bytes = fs::read(path)?;
        Ok(bytes)
    }

    fn download_bytes_isahc(&self, url: &Url) -> Result<Vec<u8>> {
        let mut resp = self.http_client.get(url.as_str())?;
        if !resp.status().is_success() {
            return Err(Error::HttpStatus(resp.status().as_u16()));
        }
        let bytes = resp.bytes()?;
        Ok(bytes)
    }
}

#[cfg(test)]
mod tests {
    use crate::Error;

    use super::BlobDownloader;
    use std::io::Write;
    use tempfile::NamedTempFile;
    use url::Url;

    static TEST_DATA: &'static [u8] = b"BlobDownloader test data";

    fn create_blobdownloader() -> BlobDownloader {
        super::BlobDownloader::new().expect("Failed to create BlobDownloader")
    }

    #[test]
    fn test_download_file() {
        let bdl = create_blobdownloader();

        let mut file = NamedTempFile::new().expect("Failed to create temporary file");
        file.write_all(TEST_DATA)
            .expect("Failed to write test data to file");

        let url = Url::from_file_path(file.path()).unwrap();
        let bytes = bdl.download_bytes(url).unwrap();
        assert_eq!(bytes, TEST_DATA);
    }

    #[test]
    fn test_download_url() {
        let bdl = create_blobdownloader();

        let url =
            Url::parse("https://httpbin.org/base64/QmxvYkRvd25sb2FkZXIgdGVzdCBkYXRh").unwrap();
        let bytes = bdl.download_bytes(url).unwrap();
        assert_eq!(bytes, TEST_DATA);
    }

    #[test]
    fn test_download_url_status() {
        let bdl = create_blobdownloader();

        let url = Url::parse("https://httpbin.org/status/404").unwrap();
        let result = bdl.download_bytes(url);
        assert!(matches!(result, Err(Error::HttpStatus(404))));
    }

    #[test]
    fn test_wrong_scheme() {
        let bdl = create_blobdownloader();

        let url = Url::parse("ftp://example.com/file.txt").unwrap();
        let result = bdl.download_bytes(url);
        match result.unwrap_err() {
            Error::InvalidScheme(s) => assert_eq!(s, "ftp"),
            err => panic!("Invalid error type: {:#}", err),
        }
    }
}
