// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use crate::CompressionMethod;
use crate::FileDefinition;
use crate::Result;
use log::info;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

enum FileDecompressor {
    LZMA(lzma::LzmaWriter<File>),
    Plain(File),
}

impl FileDecompressor {
    fn from_method(method: CompressionMethod, file: File) -> Result<Self> {
        use CompressionMethod::*;
        match method {
            LZMA => {
                let dec = lzma::LzmaWriter::new_decompressor(file)?;
                Ok(Self::LZMA(dec))
            }
            Plain => Ok(Self::Plain(file)),
        }
    }

    fn write(&mut self, data: &[u8]) -> std::io::Result<usize> {
        match self {
            Self::LZMA(wr) => wr.write_all(data),
            Self::Plain(wr) => wr.write_all(data),
        }
        .map(|_| data.len())
    }

    fn finish(self) -> Result<()> {
        match self {
            Self::LZMA(wr) => wr.finish().map(|_| ()).map_err(|e| e.into()),
            Self::Plain(_) => Ok(()),
        }
    }
}

struct PendingFile {
    last_section: Option<u32>,
    output: FileDecompressor,
}

impl PendingFile {
    fn new(output: FileDecompressor) -> Self {
        Self {
            last_section: None,
            output,
        }
    }
}

pub struct SectionUnpacker {
    output_path: std::path::PathBuf,
    files: Vec<FileDefinition>,
    section_map: BTreeMap<u32, Vec<usize>>,
    pending_files: BTreeMap<usize, PendingFile>,
}

impl SectionUnpacker {
    pub fn new(
        output_path: PathBuf,
        files: Vec<FileDefinition>,
        section_map: BTreeMap<u32, Vec<usize>>,
    ) -> Self {
        SectionUnpacker {
            output_path,
            files,
            section_map,
            pending_files: BTreeMap::new(),
        }
    }

    pub fn handle_section(&mut self, section_id: u32, data: &[u8]) -> Result<()> {
        info!("Handling section {}", section_id);

        let file_idxs = self.section_map.get(&section_id);
        if file_idxs.is_none() {
            return Ok(());
        }

        for idx in file_idxs.unwrap().iter() {
            let file = &self.files[*idx];
            let from = if section_id == file.source.start_section {
                file.source.start_offset as u64
            } else {
                0
            };
            let data_before = if section_id == file.source.start_section {
                0
            } else {
                (section_id - file.source.start_section) as u64 * file.source.section_size as u64
                    - file.source.start_offset as u64
            };
            let to = u64::min(
                from + file.source.compressed_length - data_before,
                data.len() as u64,
            );
            info!(
                "Section contains file {path} in range {from}..{to}",
                path = &file.path,
                from = from,
                to = to
            );

            if !self.pending_files.contains_key(idx) {
                info!("Creating a decompressor for file {path}", path = &file.path);
                let path = self.output_path.join(&file.path);
                std::fs::create_dir_all(path.parent().unwrap())?;
                let out_file = std::fs::File::create(path)?;
                let output = FileDecompressor::from_method(file.source.compression, out_file)?;
                self.pending_files.insert(*idx, PendingFile::new(output));
            }

            let dec = self.pending_files.get_mut(idx).unwrap();
            if let Some(last_section) = dec.last_section {
                if last_section != section_id - 1 {
                    panic!("something is wrong!");
                }
            }
            dec.output.write(&data[from as usize..to as usize])?;

            if data_before + (to - from) == file.source.compressed_length {
                info!("File {path} finished", path = &file.path);
                self.pending_files.remove(idx).unwrap().output.finish()?;
            }
        }

        Ok(())
    }
}
