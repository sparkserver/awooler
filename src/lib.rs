// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

mod blob_downloader;
mod error;
mod mirror_api;
mod safepath;
mod unpacker;
pub mod verify;
mod xml;

use std::{collections::BTreeMap, ops::RangeInclusive, path::PathBuf};

pub use blob_downloader::BlobDownloader;
use crossbeam_channel::{bounded, Receiver, Sender};
pub use error::*;
use log::info;
pub use mirror_api::MirrorAPI;
pub use unpacker::SectionUnpacker;
use url::Url;
use verify::VerifyResult;

#[derive(Debug, Copy, Clone)]
pub enum CompressionMethod {
    Plain,
    LZMA,
}

#[derive(Debug, Clone)]
pub struct FileSource {
    pub start_section: u32,
    pub start_offset: u32,
    pub compressed_length: u64,
    pub section_size: u32,
    pub compression: CompressionMethod,
}

impl FileSource {
    pub fn needed_sections(&self) -> RangeInclusive<u32> {
        let len_with_offset = self.compressed_length + (self.start_offset as u64);
        let s_size = self.section_size as u64;
        let s_count = (len_with_offset + s_size - 1) / s_size;
        self.start_section..=(self.start_section + s_count as u32 - 1)
    }
}

#[derive(Debug, Clone)]
pub enum FileHash {
    MD5([u8; 16]),
}

#[derive(Debug, Clone)]
pub struct FileDefinition {
    pub path: String,
    pub hash: FileHash,
    pub length: u64,
    pub source: FileSource,
}

#[derive(Debug, Clone)]
pub struct DeletedFile {
    pub path: String,
}

struct DownloadedSection {
    id: u32,
    data: Vec<u8>,
}

pub struct Downloader {
    bdl: BlobDownloader,
    output_path: PathBuf,
}

impl Downloader {
    pub fn new(bdl: BlobDownloader, output_path: PathBuf) -> Self {
        Self { bdl, output_path }
    }

    pub fn download(&self, mirror: Url) -> Result<()> {
        let ma = MirrorAPI::new(mirror, self.bdl.clone())?;

        let files = ma.get_index()?;
        let verify_res = verify::verify_files(&self.output_path, &files)?;

        let files: Vec<FileDefinition> = files
            .into_iter()
            .zip(verify_res)
            .filter_map(|(f, res)| match res {
                VerifyResult::Ok => None,
                _ => Some(f),
            })
            .collect();

        let mut section_map: BTreeMap<u32, Vec<usize>> = BTreeMap::new();

        for (i, file) in files.iter().enumerate() {
            for section in file.source.needed_sections() {
                if let Some(val) = section_map.get_mut(&section) {
                    val.push(i);
                } else {
                    section_map.insert(section, vec![i]);
                }
            }
        }

        let sections: Vec<u32> = section_map.keys().cloned().collect();
        let dec = SectionUnpacker::new(self.output_path.clone(), files, section_map.clone());

        let (send, recv) = bounded(32);
        std::thread::spawn(move || Self::download_thread(ma, sections, send).unwrap());

        Self::unpack_thread(dec, recv)?;

        Ok(())
    }

    fn download_thread(
        ma: MirrorAPI,
        sections: Vec<u32>,
        channel: Sender<DownloadedSection>,
    ) -> Result<()> {
        for section in sections.iter() {
            let data = ma.get_section(*section)?;
            info!("Downloaded section {}", *section);
            channel
                .send(DownloadedSection { id: *section, data })
                .unwrap();
        }
        Ok(())
    }

    fn unpack_thread(mut up: SectionUnpacker, channel: Receiver<DownloadedSection>) -> Result<()> {
        loop {
            if let Ok(section) = channel.recv() {
                up.handle_section(section.id, &section.data)?;
                info!("Unpacked section {}", section.id);
            } else {
                return Ok(());
            }
        }
    }
}
