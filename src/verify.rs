// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::{
    fs::File,
    io::{self, ErrorKind},
    path::Path,
};

use crate::{FileDefinition, FileHash, Result};

#[derive(Debug, Clone, Copy)]
pub enum VerifyResult {
    Missing,
    NotAFile,
    HashMismatch,
    Ok,
}

fn check_file_hash(path: &Path, hash: &FileHash) -> Result<VerifyResult> {
    let mut file = File::open(path)?;
    match hash {
        FileHash::MD5(want_digest) => {
            let mut hash = md5::Context::new();
            io::copy(&mut file, &mut hash)?;
            let digest = hash.compute();
            if &digest.0 == want_digest {
                Ok(VerifyResult::Ok)
            } else {
                Ok(VerifyResult::HashMismatch)
            }
        }
    }
}

pub fn verify_files(path: &Path, files: &[FileDefinition]) -> Result<Vec<VerifyResult>> {
    files
        .iter()
        .map(|f| {
            let full_path = path.join(&f.path);
            match full_path.metadata() {
                Err(e) if e.kind() == ErrorKind::NotFound => Ok(VerifyResult::Missing),
                Err(e) => Err(e.into()),
                Ok(md) if !md.is_file() => Ok(VerifyResult::NotAFile),
                Ok(md) if md.len() != f.length => Ok(VerifyResult::HashMismatch),
                Ok(_) => check_file_hash(&full_path, &f.hash),
            }
        })
        .collect::<Result<Vec<VerifyResult>>>()
}
