// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::io;

use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("http error: {0}")]
    Http(#[from] isahc::Error),
    #[error("http status: {0}")]
    HttpStatus(u16),
    #[error("io error: {0}")]
    Io(#[from] io::Error),
    #[error("XML deserialization error: {0}")]
    Xml(#[from] quick_xml::DeError),
    #[error("URL parse error: {0}")]
    InvalidUrl(#[from] url::ParseError),
    #[error("LZMA decompression error: {0}")]
    DecompressionError(#[from] lzma::LzmaError),
    #[error("invalid URL scheme: {0}")]
    InvalidScheme(String),
    #[error("invalid file URL")]
    InvalidFileURL,
    #[error("illegal file path")]
    IllegalFilePath,
    #[error("URL cannot be a base")]
    URLCannotBeABase,
}

pub type Result<T> = std::result::Result<T, Error>;
