// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use crate::{blob_downloader::BlobDownloader, xml, Error, Result};
use url::Url;

pub struct MirrorAPI {
    base_url: Url,
    bdl: BlobDownloader,
}

impl MirrorAPI {
    pub fn new(base_url: Url, blob_downloader: BlobDownloader) -> Result<Self> {
        if base_url.cannot_be_a_base() {
            Err(Error::URLCannotBeABase)
        } else {
            Ok(MirrorAPI {
                base_url,
                bdl: blob_downloader,
            })
        }
    }

    fn url_for_file(&self, file: &str) -> Url {
        let mut url = self.base_url.clone();
        {
            let mut segments = url.path_segments_mut().unwrap();
            segments.pop_if_empty(); // remove trailing slash (if there is one)
            segments.push(file);
        }
        url
    }

    pub fn get_index(&self) -> Result<Vec<crate::FileDefinition>> {
        let url = self.url_for_file("index.xml");
        let blob = self.bdl.download_bytes(url)?;
        let index = xml::parse_index(&blob)?;
        Ok(index)
    }

    pub fn get_deletions(&self) -> Result<Vec<crate::DeletedFile>> {
        let url = self.url_for_file("delete.xml");
        let blob = self.bdl.download_bytes(url)?;
        let index = xml::parse_deletions(&blob)?;
        Ok(index)
    }

    pub fn get_section(&self, section: u32) -> Result<Vec<u8>> {
        let url = self.url_for_file(&format!("section{}.dat", section));
        let blob = self.bdl.download_bytes(url)?;
        Ok(blob)
    }
}

#[cfg(test)]
mod tests {
    use crate::{BlobDownloader, Error, MirrorAPI};
    use url::Url;

    #[test]
    fn test_mirrorapi_new() {
        let bdl = BlobDownloader::new().unwrap();

        let url1 = Url::parse("mailto:test@example.com").unwrap();
        assert!(matches!(
            MirrorAPI::new(url1, bdl.clone()),
            Err(Error::URLCannotBeABase)
        ));
        let url2 = Url::parse("http://example.com").unwrap();
        assert!(matches!(MirrorAPI::new(url2, bdl.clone()), Ok(_)));
    }

    #[test]
    fn test_mirrorapi_url_for_file() {
        let bdl = BlobDownloader::new().unwrap();

        let url = Url::parse("http://example.com/some/path/").unwrap();
        let ma = MirrorAPI::new(url, bdl).unwrap();

        assert_eq!(
            ma.url_for_file("index.xml").as_str(),
            "http://example.com/some/path/index.xml"
        );
    }
}
