// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use crate::*;
use serde::Deserialize;

#[derive(Deserialize)]
struct Index {
    pub header: IndexHeader,
    #[serde(default, rename = "fileinfo")]
    pub files: Vec<IndexFile>,
}

#[derive(Deserialize)]
struct Delete {
    #[serde(default, rename = "fileinfo")]
    pub files: Vec<DeleteFile>,
}

#[derive(Deserialize)]
struct DeleteFile {
    pub path: String,
    pub file: String,
}

#[derive(Deserialize)]
#[allow(dead_code)]
struct IndexHeader {
    pub length: u64,
    pub compressed: u64,
    pub firstcab: u32,
    pub lastcab: u32,
}

fn deserialize_hash<'de, D>(d: D) -> std::result::Result<[u8; 16], D::Error>
where
    D: serde::Deserializer<'de>,
{
    let s: String = serde::de::Deserialize::deserialize(d)?;
    if s.len() != 24 {
        return Err(serde::de::Error::custom(format!(
            "unexpected encoded hash length: {} instead of {}",
            s.len(),
            24
        )));
    }
    let mut hash = [0u8; 16];
    let len = base64::decode_config_slice(&s, base64::STANDARD, &mut hash[..])
        .map_err(serde::de::Error::custom)?;
    if len != 16 {
        return Err(serde::de::Error::custom(format!(
            "unexpected decoded hash length: {} instead of {}",
            len, 16
        )));
    }
    Ok(hash)
}

#[derive(Deserialize)]
#[allow(dead_code)]
struct IndexFile {
    pub path: String,
    pub file: String,
    #[serde(deserialize_with = "deserialize_hash")]
    pub hash: [u8; 16],
    pub revision: u8,
    pub section: u32,
    pub offset: u32,
    pub length: u64,
    pub compressed: Option<u64>,
}

pub fn parse_index(bytes: &[u8]) -> Result<Vec<FileDefinition>> {
    let index: Index = quick_xml::de::from_reader(bytes)?;

    let files = index
        .files
        .into_iter()
        .map(|f| {
            Ok(FileDefinition {
                path: safepath::join_path_and_file(&f.path, &f.file)?,
                hash: FileHash::MD5(f.hash),
                length: f.length,
                source: if let Some(comp_len) = f.compressed {
                    FileSource {
                        start_section: f.section,
                        start_offset: f.offset,
                        section_size: index.header.firstcab,
                        compressed_length: comp_len,
                        compression: CompressionMethod::LZMA,
                    }
                } else {
                    FileSource {
                        start_section: f.section,
                        start_offset: f.offset,
                        section_size: index.header.firstcab,
                        compressed_length: f.length,
                        compression: CompressionMethod::Plain,
                    }
                },
            })
        })
        .collect::<std::result::Result<Vec<FileDefinition>, ()>>()
        .map_err(|_| Error::IllegalFilePath)?;
    Ok(files)
}

pub fn parse_deletions(bytes: &[u8]) -> Result<Vec<DeletedFile>> {
    let index: Delete = quick_xml::de::from_reader(bytes)?;

    let files = index
        .files
        .into_iter()
        .map(|f| {
            Ok(DeletedFile {
                path: safepath::join_path_and_file(&f.path, &f.file)?,
            })
        })
        .collect::<std::result::Result<Vec<DeletedFile>, ()>>()
        .map_err(|_| Error::IllegalFilePath)?;
    Ok(files)
}
