// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::iter;

// Implementation of this module is based on https://learn.microsoft.com/en-us/windows/win32/fileio/naming-a-file

static RESERVED_FILENAMES: &[&str] = &[
    "CON", "PRN", "AUX", "NUL", "COM0", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7",
    "COM8", "COM9", "LPT0", "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9",
];

static RESERVED_CHARS: &[char] = &['<', '>', ':', '"', '/', '\\', '|', '?', '*'];

fn is_reserved_name(pc: &str) -> bool {
    let without_ext = pc.split('.').nth(0).unwrap();
    let without_ext_upper = without_ext.to_ascii_uppercase();
    RESERVED_FILENAMES.contains(&without_ext_upper.as_str())
}

fn contains_reserved_characters(pc: &str) -> bool {
    pc.chars().any(|ch| {
        if ch < ' ' {
            return true;
        }

        RESERVED_CHARS.contains(&ch)
    })
}

fn is_illegal_component(pc: &str) -> bool {
    if pc.ends_with('.') {
        return true;
    }
    if contains_reserved_characters(pc) {
        return true;
    }
    if is_reserved_name(pc) {
        return true;
    }
    false
}

pub fn join_path_and_file(path: &str, file: &str) -> Result<String, ()> {
    // file can't be empty
    if file == "" {
        return Err(());
    }

    // split to path components
    let iter_a = path.split('/');
    let iter_b = iter::once(file);
    let components: Vec<&str> = iter_a
        .chain(iter_b) // concatenate
        .filter(|p| p.len() > 0) // filter out empty components
        .collect();
    for pc in components.iter() {
        if is_illegal_component(pc) {
            return Err(());
        }
    }
    Ok(components.join("/"))
}

#[cfg(test)]
mod tests {
    use super::{is_illegal_component, join_path_and_file};

    #[test]
    fn test_is_illegal_component() {
        let illegal_tests = [
            "\x00.txt",
            "cOm1.txt",
            "hello/world.txt",
            "c:file.txt",
            "hello\\file.txt",
            "..",
            ".",
        ];
        for test in illegal_tests {
            assert_eq!(is_illegal_component(test), true);
        }
    }

    #[test]
    fn test_join_path_and_file() {
        assert_eq!(
            join_path_and_file("Root/Dir1", "file.txt"),
            Ok("Root/Dir1/file.txt".to_string())
        );

        let illegal_tests = [("C:Root/Dir1", "file.txt"), ("Root/../../..", "file.txt")];

        for (path, file) in illegal_tests {
            assert_eq!(join_path_and_file(path, file), Err(()));
        }
    }
}
