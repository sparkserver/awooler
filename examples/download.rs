// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::{error::Error, path::PathBuf};

use argh::FromArgs;
use awooler::{BlobDownloader, Downloader};
use url::Url;

#[derive(FromArgs)]
/// CLI downloader
struct Args {
    /// mirror URL (supported schemes: file, http, https)
    #[argh(option, short = 'm')]
    pub mirror: String,

    /// download output path
    #[argh(option, short = 'o')]
    pub output_path: PathBuf,
}

type Result<T> = std::result::Result<T, Box<dyn Error>>;

fn main() -> Result<()> {
    let env = env_logger::Env::new().default_filter_or("info");
    env_logger::init_from_env(env);

    let args: Args = argh::from_env();
    let base_url = Url::parse(&args.mirror)?;
    let bdl = BlobDownloader::new()?;

    let downloader = Downloader::new(bdl, args.output_path);
    downloader.download(base_url)?;

    Ok(())
}
